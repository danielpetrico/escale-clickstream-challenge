#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from pyspark.sql import SparkSession
from pyspark.sql import functions as func
from pyspark.sql import Window
from pyspark.sql.types import TimestampType, FloatType

from datetime import datetime
import numpy as np


# In[ ]:


spark = SparkSession.builder.appName("Escale_ClickStream_Analytics").getOrCreate()


# In[ ]:


path = '/home/daniel/pessoal/escale/arquivos/part-00000.json'
sessionsDF = spark.read.json(path)


# In[ ]:


windowTimestamp = Window.orderBy('device_sent_timestamp').partitionBy('anonymous_id')

previous_timestamp_col = func.lag('device_sent_timestamp').over(windowTimestamp)
session_duration_col = (func.col('device_sent_timestamp') - func.col('previous_timestamp')) / 1000 / 60
session_new_col = func.when(func.col('session_duration') > 30, 1).otherwise(0)
session_id_col = func.concat('anonymous_id', func.lit('-'), func.sum(func.col('session_new')).over(windowTimestamp))

filteredDF = sessionsDF     .withColumn('previous_timestamp', previous_timestamp_col)     .withColumn('session_duration', func.coalesce(session_duration_col, func.lit(0)))     .withColumn('session_new', session_new_col)     .withColumn('session_id', session_id_col)     .orderBy('device_sent_timestamp')


# In[ ]:


def median(values_list):
    med = np.median(values_list)
    return float(med)

udf_median = func.udf(median, FloatType())


# In[ ]:


windowBrowser = Window.partitionBy('session_id', 'browser_family')

duration_tot_browser_col = func.sum('session_duration').over(windowBrowser)

aggDF = filteredDF     .withColumn('duration_tot_browser', duration_tot_browser_col) 

browserDF = aggDF.select('browser_family', 'duration_tot_browser')     .distinct()     .groupby('browser_family')     .agg(udf_median(func.collect_list(func.col('duration_tot_browser'))).alias('browser_family_median'))


# In[ ]:


browserDF.show()

